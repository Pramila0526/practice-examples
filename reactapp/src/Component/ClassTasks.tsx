
import React from 'react';

 interface Employees{
     name : String
 }

 interface EmpListProps{
    initialTasks : Employees[]
 }

 interface EmpListStates{
     emps: Employees[]
 }

 class EmpsList extends React.Component<
 EmpListProps,
 EmpListStates 
 >{
     constructor(props : EmpListProps){
         super(props);
         this.state = {
          emps: props.initialTasks
         };

         //Binding onAddNewTasksClick() in Constructor
         //This way we make sure that the instance object is bound to this(onAddNewTasksClick) method
         this.onAddNewEmployeesClick = this.onAddNewEmployeesClick.bind(this)
     }

     onAddNewEmployeesClick() {
        this.setState({
            emps: [
                ...this.state.emps,
                {name : 'Employee Name'}
            ]
        });
     }

    render() {
        const { emps } = this.state;
        return (
            <ul> 
                {emps.map((emp,i) => {
                    return <li key={i}>{emp.name}</li>;
                })}
                <button onClick={this.onAddNewEmployeesClick}>Add New Employees</button>
            </ul>
         );
    }
 
 }
 const emps = [
     {name : 'Employee1'},
     {name : 'Employee2'},
     {name : 'Employee3'},
     {name : 'Employee5'}
 ];

export default () => (
    <div>
        <EmpsList initialTasks={emps}/>
            </div>
)