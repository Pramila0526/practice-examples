import React from 'react';
import logo from './logo.svg';
import './App.css';
import ClassTasks from './Component/ClassTasks';

function App() {
   return (
      <div>
         <ClassTasks/>
      </div>
  );
 }

export default App;
