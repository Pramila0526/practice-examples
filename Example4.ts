// Typescript doesn't support any type of 
// {} is same as object

// const person: {
//     name: string,
//     age : number
// } = {
//     name : "Pramila",
//     age : 22
// } //This is not a good practice // Thisis called type object

// This is for tupe Example
// const person :{
//     name : string,
//     age : number,
//     hobbies : string[],
//     role : [number,string] // this is tuple
//  // this tupple tells that i want a special array with exactly 2  first is number and second is string
// } = 

// const ADMIN = 0;
// const READ_ONLY = 1;
// const AUTHOR = 2;

enum Rights  {ADMIN, READ_ONLY, AUTHOR}
const person = { 
    name : 'Pramila',
    age : 22,
    hobbies : ['Cooking','Drawing'], // Array inside the object
    role : [2 , 'Author'],
    rights : Rights.ADMIN   // enum
}

person.role.push('admin') // push is actually an exception which is allowed in tuple hence it is not showing any error
// person.role[1] = 10; // This is giving arror because as specified  in role tuple we cannot have sting in second position (i.e role[1])

person.role = [1,'admin'] // here we can only take 2 parameters as specified in the tuple

let arrayOfHobbies : string[]  // we can also use any[] but in typescript it is not a good practice
arrayOfHobbies = ['Sports','Swimming']

// console.log(person);

// For loop of typescript
for(const hobby of person.hobbies){
       console.log(hobby.toUpperCase());
    // console.log(hobby.map());  We are getting error because map doesn't works for string it works for array  
}

if(person.rights === Rights.AUTHOR){  // enum practice
    console.log("It is Author");
}
