function add(n1: number, n2:number, showResult: boolean, phrase:string){
    // console.log(typeof n1);
    // This is vanilla js
    // In typescrpit there is no need to perform vanilla typescript 
    //if(typeof n1 !== 'number' || typeof n2 !== 'number'){
    //    throw new Error ("Incorrect Input");     
    // }
    const result = n1+n2
    if(showResult){
        console.log(phrase + result);
        
    }
    else{
        return result
    }   
}

 // const number1 = "5"

const number1 = 5
const number2 = 7.8
const printResult = true
const resultPhrase = 'Result is'

add(number1,number2,printResult,resultPhrase) 
// here we are getting error because the data type of n1 is number and we assign it a string (as commented)


